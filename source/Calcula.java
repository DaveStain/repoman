
public class Calcula {
    
    public static double Suma(double a, double b){
        double c;
        c=a+b;
        return c;
    }
    
    public static double Resta(double a, double b){
        double c;
        c=a-b;
        return c;
    }
    
    public static double Multi(double a, double b){
        double c;
        c=a*b;
        return c;
    }
    
    public static double Division(double a, double b){
        double c;
        c=a/b;
        return c;
    }
    
    public static double Seno(double a){
        double c;
        c = Math.sin(Math.toRadians(a));
        return c;
    }
    
    public static double Coseno(double a, double b){
        double c;
        c = Math.cos(Math.toRadians(a));
        return c;
    }
    
    public static double Tangente(double a, double b){
        double c;
        c = Math.tan(Math.toRadians(a));
        return c;
    }
    
}
