import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class PruebaCalc{

	@Test
	public void testCalc(){
		Calcula cal = new Calcula();
		double result = cal.Suma(1,1);
		assertEquals(2,result,0);
		result = cal.Resta(1,1);
		assertEquals(0,result,0);
		result = cal.Multi(2,2);
		assertEquals(4,result,0);
		result = cal.Division(6,2);
		assertEquals(3,result,0);
	}
}